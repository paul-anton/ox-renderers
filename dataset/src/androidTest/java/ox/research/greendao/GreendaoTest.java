package ox.research.greendao;

import static org.junit.Assert.assertEquals;

import android.database.sqlite.SQLiteDatabase;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ox.MockEntityFactory;
import ox.dto.DaoMaster;
import ox.dto.DaoSession;
import ox.dto.MockEntityDto;
import ox.dto.MockEntityDtoDao;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class GreendaoTest {

    MockEntityDtoDao mockEntityDtoDao;

    @Before
    public void setup() {
        final SQLiteDatabase database = SQLiteDatabase.create(null);
        DaoMaster daoMaster = new DaoMaster(database);
        daoMaster.createAllTables(daoMaster.getDatabase(),false);
        DaoSession daoSession = daoMaster.newSession();
        mockEntityDtoDao = daoSession.getMockEntityDtoDao();
    }

    @Test
    public void shouldInsertSpecificSize() {
        int size = 100000;
        mockEntityDtoDao.insertInTx(MockEntityFactory.mockEntityDtos(size));
        final List<MockEntityDto> mockEntityDtos = mockEntityDtoDao.loadAll();
        assertEquals(size, mockEntityDtos.size());
    }

}
