package ox.dagger;

import dagger.Component;

import javax.inject.Singleton;

import ox.MainActivity;

@Component
@Singleton
public interface DaggerComponent {

    void inject(MainActivity mainActivity);

}
