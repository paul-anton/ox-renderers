package ox.dagger;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

import ox.dto.DaoMaster;
import ox.dto.DaoSession;

@Module
public class DaggerModule {

    @Provides
    @Singleton
    public DaoSession getDaoSession(Context context) {
        DaoMaster.DevOpenHelper openHelper = new DaoMaster.DevOpenHelper(context, "db", null);
        SQLiteDatabase database = openHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(database);
        return daoMaster.newSession();
    }

}
