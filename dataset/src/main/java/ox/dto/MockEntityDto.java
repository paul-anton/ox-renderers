package ox.dto;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "MOCK_ENTITY_DTO".
 */
@Entity
public class MockEntityDto {
    private String mockSubject;
    private String mockSender;
    private String mockDate;

    @Generated
    public MockEntityDto() {
    }

    @Generated
    public MockEntityDto(String mockSubject, String mockSender, String mockDate) {
        this.mockSubject = mockSubject;
        this.mockSender = mockSender;
        this.mockDate = mockDate;
    }

    public String getMockSubject() {
        return mockSubject;
    }

    public void setMockSubject(String mockSubject) {
        this.mockSubject = mockSubject;
    }

    public String getMockSender() {
        return mockSender;
    }

    public void setMockSender(String mockSender) {
        this.mockSender = mockSender;
    }

    public String getMockDate() {
        return mockDate;
    }

    public void setMockDate(String mockDate) {
        this.mockDate = mockDate;
    }

}
