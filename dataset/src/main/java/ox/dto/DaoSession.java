package ox.dto;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig mockEntityDtoDaoConfig;

    private final MockEntityDtoDao mockEntityDtoDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        mockEntityDtoDaoConfig = daoConfigMap.get(MockEntityDtoDao.class).clone();
        mockEntityDtoDaoConfig.initIdentityScope(type);

        mockEntityDtoDao = new MockEntityDtoDao(mockEntityDtoDaoConfig, this);

        registerDao(MockEntityDto.class, mockEntityDtoDao);
    }
    
    public void clear() {
        mockEntityDtoDaoConfig.getIdentityScope().clear();
    }

    public MockEntityDtoDao getMockEntityDtoDao() {
        return mockEntityDtoDao;
    }

}
