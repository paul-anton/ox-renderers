package ox;


import android.database.sqlite.SQLiteDatabase;

import java.io.File;

public class DbMeasureHelper {
    public static boolean hasAtLeastMBSize(SQLiteDatabase database, long size) {
        long bytes = new File(database.getPath()).length();
        System.out.println("bytes: " + bytes);
        double tentosixth = Math.pow(10,6);
        double megabytes = bytes / tentosixth;
        System.out.println("megabytes: " + megabytes);
        return size > megabytes;
    }
}
