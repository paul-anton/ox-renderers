package ox;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;

import ox.research.R;
import ox.dto.DaoMaster;
import ox.dto.DaoSession;
import ox.dto.MockEntityDtoDao;

public class MainActivity extends AppCompatActivity {

    //    @Inject
    DaoSession daoSession;

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    RecyclerCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaoMaster.DevOpenHelper openHelper = new DaoMaster.DevOpenHelper(this, "db", null);
        SQLiteDatabase database = openHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();

        final MockEntityDtoDao mockEntityDtoDao = daoSession.getMockEntityDtoDao();

        while (DbMeasureHelper.hasAtLeastMBSize(database, 10)) {
            mockEntityDtoDao.insertInTx(MockEntityFactory.mockEntityDtos(10000));
        }

        database.beginTransaction();
        String selectAllQuery = String.format("SELECT rowid _id, * FROM %s", "MOCK_ENTITY_DTO");

        final Cursor cursor = database.rawQuery(selectAllQuery, null);
        try {
            if (cursor.moveToFirst()) {
                database.setTransactionSuccessful();
            }
        } finally {
            /*if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }*/
        }
        database.endTransaction();

        adapter = new RecyclerCursorAdapter(this, cursor);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        System.out.println("cursor count: " + cursor.getCount());

        /*
        final List<MockEntityDto> mockEntityDtos = mockEntityDtoDao.loadAll();

        adapter = new RecyclerCursorAdapter(mockEntityDtos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        */
    }
}
