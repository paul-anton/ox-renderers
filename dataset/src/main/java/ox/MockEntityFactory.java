package ox;

import java.util.LinkedList;
import java.util.List;

import ox.dto.MockEntityDto;

public class MockEntityFactory {

    public static List<MockEntityDto> mockEntityDtos(int howMany) {
        List<MockEntityDto> result = new LinkedList<>();
        for (int i = 0; i < howMany; i++) {
            String subject = new StringBuilder().append("Subject subject subject subject subject ").append(i).toString();
            String sender = new StringBuilder().append("Sender sender sender sender sender sender").append(i).toString();
            String date = new StringBuilder().append("Date").append(i).toString();
            MockEntityDto mockEntityDto = new MockEntityDto();
            mockEntityDto.setMockSubject(subject);
            mockEntityDto.setMockSender(sender);
            mockEntityDto.setMockDate(date);
            result.add(mockEntityDto);
        }
        return result;
    }
}
