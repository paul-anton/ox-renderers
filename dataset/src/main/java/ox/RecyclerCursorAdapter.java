package ox;


import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

import ox.research.R;

public class RecyclerCursorAdapter extends RecyclerView.Adapter<RecyclerCursorAdapter.EntityViewHolder>{

    private Context context;

    CursorAdapter cursorAdapter;

    public RecyclerCursorAdapter(Context context, Cursor cursor) {
        this.context = context;
        cursorAdapter = new CursorAdapter(context, cursor) {
            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_entity, parent, false);
                return view;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {
                TextView subject = ButterKnife.findById(view, R.id.mail_subject);
                TextView sender = ButterKnife.findById(view, R.id.mail_sender);
                TextView date = ButterKnife.findById(view, R.id.mail_date);
                subject.setText(cursor.getString(cursor.getColumnIndex("MOCK_SUBJECT")));
                sender.setText(cursor.getString(cursor.getColumnIndex("MOCK_SENDER")));
                date.setText(cursor.getString(cursor.getColumnIndex("MOCK_DATE")));
            }
        };
    }


    @Override
    public EntityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = cursorAdapter.newView(context, cursorAdapter.getCursor(), parent);
        return new EntityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final EntityViewHolder holder, final int position) {
        cursorAdapter.getCursor().moveToPosition(position);
        cursorAdapter.bindView(holder.itemView, context, cursorAdapter.getCursor());
    }

    @Override
    public int getItemCount() {
        return cursorAdapter.getCount();
    }

    public class EntityViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.mail_subject)
        TextView mailSubject;

        @Bind(R.id.mail_sender)
        TextView mailSender;

        @Bind(R.id.mail_date)
        TextView mailDate;

        public EntityViewHolder(View itemView) {
            super(itemView);
            //ButterKnife.bind(this,itemView);
        }
    }

}
