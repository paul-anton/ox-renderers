package ox.mobile;


import de.greenrobot.common.Base64;

import java.security.GeneralSecurityException;
import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.inject.Inject;

import ox.mobile.dagger.Dagger;

public class Encryption {

    private static final String ALIAS = "USER&PASSWORDENCRYPTIONKEY";

    @Inject
    SecretKeyWrapper secretKeyWrapper;

    private KeyStore.PrivateKeyEntry privateKeyEntry;

    public Encryption() {
        Dagger.getComponent().inject(this);
        try {
            privateKeyEntry = secretKeyWrapper.getPrivateKey(ALIAS);
        } catch (GeneralSecurityException e) {
            System.out.println(e.getStackTrace());
        }
    }

    public String encrypt(String plainText) {
        try {
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getPrivateKey());
            byte[] utf8 = plainText.getBytes("UTF-8");
            byte[] encryptedArray = cipher.doFinal(utf8);

            return Base64.encodeBytes(encryptedArray);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public String decrypt(String cipherText) {
        try {
            byte[] cipherTextArray = Base64.decode(cipherText);
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
            return new String(cipher.doFinal(cipherTextArray), "UTF-8");
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

}
