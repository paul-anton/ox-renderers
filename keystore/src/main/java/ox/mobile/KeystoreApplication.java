package ox.mobile;

import android.app.Application;

import ox.mobile.dagger.Dagger;
import ox.mobile.dagger.DaggerMainDaggerComponent;

public class KeystoreApplication extends Application {

    static KeystoreApplication instance;

    public static KeystoreApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        setupInstance();
        super.onCreate();
        setupDagger();
    }

    private void setupInstance() {
        instance = this;
    }

    private void setupDagger() {
        Dagger.initalize(DaggerMainDaggerComponent.create());
    }
}
