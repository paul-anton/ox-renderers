package ox.mobile.dagger;


public class Dagger {

    static MainDaggerComponent component;

    public static void initalize(MainDaggerComponent mainDaggerComponent) {
        component = mainDaggerComponent;
    }

    public static MainDaggerComponent getComponent() {
        if (component == null) {
            component = DaggerMainDaggerComponent.create();
        }
        return component;
    }
}
