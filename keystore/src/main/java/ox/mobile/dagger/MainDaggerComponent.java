package ox.mobile.dagger;


import dagger.Component;

import javax.inject.Singleton;

import ox.mobile.AuthenticationPersistence;
import ox.mobile.Encryption;
import ox.mobile.KeystoreApplication;
import ox.mobile.MainActivity;

@Singleton
@Component(modules = MainDaggerModule.class)
public interface MainDaggerComponent {

    void inject(AuthenticationPersistence authenticationPersistence);

    void inject(MainActivity mainActivity);

    void inject(Encryption encryption);

    KeystoreApplication getApplication();
}
