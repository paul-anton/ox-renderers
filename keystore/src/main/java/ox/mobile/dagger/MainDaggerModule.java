package ox.mobile.dagger;


import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

import ox.mobile.AuthenticationPersistence;
import ox.mobile.Encryption;
import ox.mobile.KeystoreApplication;
import ox.mobile.SecretKeyWrapper;

@Module
class MainDaggerModule {

/*    @Provides
    @Singleton
    public Context getContext() {
        return KeystoreApplication.get();
    }*/

    @Provides
    KeystoreApplication getApplication() {
        return KeystoreApplication.get();
    }

    @Provides
    @Singleton
    public SharedPreferences getSharedAuthenticationPreferences(KeystoreApplication context) {
        return context.getSharedPreferences(AuthenticationPersistence.IDENTIFIER, Context.MODE_PRIVATE);

    }

    @Provides
    @Singleton
    public AuthenticationPersistence getAuthenticationPersistence() {
        return new AuthenticationPersistence();
    }

    @Provides
    @Singleton
    public Encryption getEncryption() {
        return new Encryption();
    }

    @Provides
    @Singleton
    public SecretKeyWrapper getSecretKeyWrapper(KeystoreApplication context) {
        return new SecretKeyWrapper(context);
    }

}
