package ox.mobile;

import android.content.SharedPreferences;

import javax.inject.Inject;

import ox.mobile.dagger.Dagger;

public class AuthenticationPersistence {

    public final static String IDENTIFIER = "AuthenticationPersistence";

    private final static String USERNAME = "username";

    private final static String PASSWORD = "password";

/*    @Inject
    @Named("authentication")*/
    SharedPreferences sharedPreferences;

    @Inject
    Encryption encryption;

    public AuthenticationPersistence() {
        Dagger.getComponent().inject(this);
    }

    public void storeUsernameAndPassword(String username, String password) {
        String encryptedUsername = encryption.encrypt(username);
        String encryptedPassword = encryption.encrypt(password);
        /*String encryptedUsername = username;
        String encryptedPassword = password;*/
        sharedPreferences.edit()
                .putString(USERNAME, encryptedUsername)
                .putString(PASSWORD, encryptedPassword)
                .apply();
    }

    public String getUsername() {
        String username = sharedPreferences.getString(USERNAME, "");
        return username.equals("") ? "" : encryption.decrypt(username);
        //return username.equals("") ? "" : username;
    }

    public String getPassword() {
        String password = sharedPreferences.getString(PASSWORD, "");
        return password.equals("") ? "" : encryption.decrypt(password);
        //return password.equals("") ? "" : password;
    }

    public void clearUsernameAndPassword() {
        sharedPreferences.edit()
                .putString(USERNAME, "")
                .putString(PASSWORD, "")
                .apply();
    }


}
