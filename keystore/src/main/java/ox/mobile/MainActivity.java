package ox.mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import javax.inject.Inject;

import ox.mobile.dagger.Dagger;

public class MainActivity extends AppCompatActivity {

    @Inject
    AuthenticationPersistence authenticationPersistence;
/*
    @Inject
    SharedPreferences sharedPreferences;*/

    @Bind(R.id.entry_name)
    EditText username;

    @Bind(R.id.entry_password)
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Dagger.getComponent().inject(this);
    }


    @OnClick(R.id.generate_button)
    public void onLogin() {
        //final SharedPreferences.Editor edit = sharedPreferences.edit();
        String user = username.getText().toString();
        String pass = password.getText().toString();

        if (user == null || user.length() == 0) {
            user = "user";
        }

        if (pass == null || pass.length() == 0) {
            pass = "pass";
        }
        authenticationPersistence.storeUsernameAndPassword(user, pass);
    }
}
