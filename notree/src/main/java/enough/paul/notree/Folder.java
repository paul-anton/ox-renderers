package enough.paul.notree;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import lombok.Data;
import lombok.Getter;

@Data
public class Folder {

    long id;

    String title;

    private static int lastFolderId = 0;

    @Getter
    List<Folder> children = new LinkedList<>();

    public Folder(long id, String title) {
        this.id = id;
        this.title = title;
    }


    public static ArrayList<Folder> createFoldersList(int numFolders, boolean children) {
        ArrayList<Folder> folders = new ArrayList<Folder>();

        if (children) {
            for (int i = 1; i <= numFolders; i++) {
                folders.add(new Folder(lastFolderId, "Child Folder " + lastFolderId++));
            }
        } else {
            for (int i = 1; i <= numFolders; i++) {
                if (i % 2 == 0) {
                    folders.add(new Folder(lastFolderId, "Empty Folder " + lastFolderId++));
                } else {
                    final Folder parentFolder = new Folder(lastFolderId, "Parent Folder " + lastFolderId++);
                    parentFolder.setChildren(createFoldersList(2, true));
                    folders.add(parentFolder);
                }
            }
        }
        return folders;
    }

}
