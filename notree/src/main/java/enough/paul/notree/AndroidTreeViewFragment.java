package enough.paul.notree;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;

import enough.paul.notree.fromRenderers.Email;

/**
 * Created by Paul on 14/07/2016.
 */

public class AndroidTreeViewFragment extends Fragment{
    @Bind(R.id.treeview_container)
    ViewGroup containerView;

    @Bind(R.id.status_bar)
    TextView helloTv;

    ArrayList<Email> emails;

    private AndroidTreeView treeView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_treeview, null, false);
        ButterKnife.bind(this, rootView);
        helloTv.setText("Tree View");
        setupNodes();

        return rootView;
    }

    private void setupNodes() {
        TreeNode root = TreeNode.root();

        ArrayList<TreeNode> emailNodes = new ArrayList<>();

        for (Email email: emails) {
            TreeNode emailNode = new TreeNode(email).setViewHolder(new EmailItemViewHolder(getActivity()));
            populateEmailNodeWithSubitems(emailNode);
            emailNodes.add(emailNode);
        }

        root.addChildren(emailNodes);

        setupTreeView(root);
    }

    private void populateEmailNodeWithSubitems(TreeNode emailNode) {
        Email email = (Email) emailNode.getValue();
        TreeNode subject = new TreeNode("Subject " + email.getSubject()).setViewHolder(new EmailSubItemViewHolder(getActivity()));
        TreeNode sender = new TreeNode("Sender " + email.getSender()).setViewHolder(new EmailSubItemViewHolder(getActivity()));

        emailNode.addChild(subject);
        emailNode.addChild(sender);
    }

    private void setupTreeView(TreeNode root){
        treeView = new AndroidTreeView(getActivity(), root);
        containerView.addView(treeView.getView());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupData();
    }

    private void setupData() {
        emails = Email.createEmailsList(20);
    }

}
