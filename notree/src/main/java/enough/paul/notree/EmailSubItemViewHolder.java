package enough.paul.notree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;

import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Paul on 14/07/2016.
 */

public class EmailSubItemViewHolder extends TreeNode.BaseNodeViewHolder<String> {

    public EmailSubItemViewHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, String value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_item_sub_email, null, false);
        TextView content = ButterKnife.findById(view, R.id.text);
        content.setText(value);
        return view;
    }
}
