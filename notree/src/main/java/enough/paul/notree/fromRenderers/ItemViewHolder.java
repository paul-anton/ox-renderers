package enough.paul.notree.fromRenderers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import lombok.Data;

import enough.paul.notree.R;


@Data
public class ItemViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.view_item_top)
    View removableView;

    ItemActionClickListener itemActionClickListener;

    public ItemViewHolder(View itemView, ItemActionClickListener itemActionClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.itemActionClickListener = itemActionClickListener;
    }

    public interface ItemActionClickListener {
        void onAddClicked(int position);

        void onMailClicked(int position);

        void onMailSenderClicked(int position);
    }

    /*@OnClick(R.id.mail_add_button)
    void onAddClicked() {
        itemActionClickListener.onAddClicked(getAdapterPosition() + 1);
    }
*/
    @OnClick(R.id.view_item_top)
    void onMailClicked() {
        itemActionClickListener.onMailClicked(getAdapterPosition());
    }

    @OnClick(R.id.mail_sender)
    void onMailSenderClicked() {
        itemActionClickListener.onMailSenderClicked(getAdapterPosition());
    }

    // getAdapterPosition becomes not available

}
