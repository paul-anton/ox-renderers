package enough.paul.notree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;

import com.unnamed.b.atv.model.TreeNode;

import enough.paul.notree.fromRenderers.Email;

/**
 * Created by Paul on 14/07/2016.
 */

public class EmailItemViewHolder extends TreeNode.BaseNodeViewHolder<Email>{

    public EmailItemViewHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, Email email) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_item_email, null, false);

        TextView sender = ButterKnife.findById(view, R.id.mail_sender);
        TextView subject = ButterKnife.findById(view, R.id.mail_subject);

        sender.setText(email.getSender());
        subject.setText(email.getSubject());

        return view;
    }
}
