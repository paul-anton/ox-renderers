package enough.paul.notree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.listView)
    ListView listView;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Bind(R.id.drawer_treeview_container)
    ViewGroup drawerTreeViewContainer;

    private AndroidTreeView treeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        final LinkedHashMap<String, Class<?>> listItems = new LinkedHashMap<>();
        listItems.put("Recycler View ", RecyclerViewFragment.class);
        listItems.put("AndroidTreeView", AndroidTreeViewFragment.class);


        final ArrayList listOfFragmentClassNames = new ArrayList(listItems.keySet());
        final SimpleArrayAdapter adapter = new SimpleArrayAdapter(this, listOfFragmentClassNames);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Class<?> clazz = listItems.values().toArray(new Class<?>[]{})[i];
                Intent intent = new Intent(MainActivity.this, SingleFragmentActivity.class);
                intent.putExtra(SingleFragmentActivity.FRAGMENT_PARAM, clazz);
                MainActivity.this.startActivity(intent);
            }
        });


        setupDrawerNodes();

    }

    private class SimpleArrayAdapter extends ArrayAdapter<String> {
        public SimpleArrayAdapter(Context context, List<String> objects) {
            super(context, android.R.layout.simple_list_item_1, objects);

        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private void setupDrawerNodes() {
        TreeNode root = TreeNode.root();

        ArrayList<TreeNode> folderNodes = new ArrayList<>();
        ArrayList<Folder> folders = Folder.createFoldersList(5, false);
        for (Folder folder : folders) {
            TreeNode folderNode = new TreeNode(folder).setViewHolder(new FolderItemViewHolder(this));
            populateEmailNodeWithSubitems(folderNode);
            folderNodes.add(folderNode);
        }

        root.addChildren(folderNodes);

        setupTreeView(root);
    }

    private void populateEmailNodeWithSubitems(TreeNode folderNode) {
        Folder folder = (Folder) folderNode.getValue();
        List<Folder> subfolders = folder.getChildren();
        List<TreeNode> subfoldersNodes = new ArrayList<>();
        for (Folder subfolder : subfolders) {
            subfoldersNodes.add(new TreeNode(subfolder).setViewHolder(new FolderItemViewHolder(this)));
        }

        folderNode.addChildren(subfoldersNodes);
    }

    private void setupTreeView(TreeNode root) {
        treeView = new AndroidTreeView(this, root);
        treeView.setDefaultAnimation(true);
        treeView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        drawerTreeViewContainer.addView(treeView.getView());
    }


}
