package enough.paul.notree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;

import com.unnamed.b.atv.model.TreeNode;


public class FolderItemViewHolder extends TreeNode.BaseNodeViewHolder<Folder>{

    public FolderItemViewHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, Folder folder) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_item_folder, null, false);

        TextView title = ButterKnife.findById(view, R.id.folder_title);

        title.setText(folder.getTitle());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tView.toggleNode(node);
            }
        });

        return view;
    }
}
