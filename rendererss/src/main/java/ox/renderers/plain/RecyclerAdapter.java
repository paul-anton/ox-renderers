package ox.renderers.plain;


import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;

import java.util.ArrayList;
import java.util.List;

import ox.renderers.ItemTouchAdapter;
import ox.renderers.R;
import ox.renderers.model.Email;

public class RecyclerAdapter extends RecyclerView.Adapter<ItemViewHolder> implements ItemTouchAdapter, ItemViewHolder.ItemActionClickListener {

    private List<Email> emails = new ArrayList<>();

    public RecyclerAdapter(ArrayList<Email> emailsList) {
        emails = emailsList;
    }

    private Context context;

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context == null) {
            context = parent.getContext();
        }
        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_email_simplified, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_email, parent, false);
        return new ItemViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        TextView sender = ButterKnife.findById(holder.itemView, R.id.mail_sender);
        TextView subject = ButterKnife.findById(holder.itemView, R.id.mail_subject);
        //RelativeLayout top_layout = ButterKnife.findById(holder.itemView, R.id.view_item_top);
        LinearLayout top_layout = ButterKnife.findById(holder.itemView, R.id.view_item_top);

        Email email = emails.get(position);
        sender.setText(email.getSubject());
        subject.setText(email.getSender());
        if (email.isRead()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                subject.setTextAppearance(context, R.style.email_subject_read);
                sender.setTextAppearance(context, R.style.email_sender_read);
                top_layout.setBackgroundColor(context.getResources().getColor(R.color.list_item_top_layout_background_pressed));
            } else {
                subject.setTextAppearance(R.style.email_subject_read);
                sender.setTextAppearance(R.style.email_sender_read);
                top_layout.setBackgroundColor(ContextCompat.getColor(context, R.color.list_item_top_layout_background_pressed));
            }
        }
    }

    @Override
    public int getItemCount() {
        return emails.size();
    }

    @Override
    public void onItemLongPressed(int position) {
        if (position != RecyclerView.NO_POSITION) {
            emails.remove(emails.get(position));
            notifyItemRemoved(position);
        }
    }

    @Override
    public void onAddClicked(int position) {
        emails.add(position, Email.createEmail());
        notifyItemInserted(position);
    }

    @Override
    public void onMailClicked(int position) {
        if (position != RecyclerView.NO_POSITION) {
            Toast.makeText(context, "Clicked on mail with subject " + emails.get(position).getSubject(), Toast.LENGTH_SHORT).show();
            emails.get(position).setRead(true);
            notifyItemChanged(position);
        }
    }

    @Override
    public void onMailSenderClicked(int position) {
        if (position != RecyclerView.NO_POSITION) {
            Toast.makeText(context, "Clicked on sender " + emails.get(position).getSender(), Toast.LENGTH_SHORT).show();
        }
    }

    public void reset() {
        emails.clear();
        emails.addAll(Email.createEmailsList(20));
        notifyDataSetChanged();
    }

}
