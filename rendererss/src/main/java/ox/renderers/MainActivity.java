package ox.renderers;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;

import ox.renderers.model.Email;
import ox.renderers.plain.CustomSimpleCallback;
import ox.renderers.plain.RecyclerAdapter;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.swipeContainer)
    SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupToolbar();
        setupAdapter();
        setupRecyclerView();
        setupTouchHelper();
        setupSwipeRefreshLayout();
    }

    private void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    private void setupAdapter() {
        adapter = new RecyclerAdapter(Email.createEmailsList(20));
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setupTouchHelper() {
        CustomSimpleCallback customSimpleCallback = new CustomSimpleCallback(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(customSimpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.reset();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option_add_bottom:
                Toast.makeText(this, "Adding on bottom ", Toast.LENGTH_SHORT).show();
                adapter.onAddClicked(adapter.getItemCount());
                break;

            case R.id.option_add_top:
                Toast.makeText(this, "Adding on top ", Toast.LENGTH_SHORT).show();
                adapter.onAddClicked(0);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }
}
