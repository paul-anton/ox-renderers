package ox.renderers;


public interface ItemTouchAdapter {

    void onItemLongPressed(int position);

}
