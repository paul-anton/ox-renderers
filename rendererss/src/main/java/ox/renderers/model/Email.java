package ox.renderers.model;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Email {
    private String subject;
    private String sender;
    private boolean read;

    public Email(String subject, String sender) {
        this.subject = subject;
        this.sender = sender;
        this.read = false;
    }

    private static int lastEmailId = 0;

    public static ArrayList<Email> createEmailsList(int numEmails) {
        ArrayList<Email> emails = new ArrayList<Email>();

        for (int i = 1; i <= numEmails; i++) {
            if (i % 5 == 0) {
                emails.add(new Email("How to handle loooooooooong subject lines? " + lastEmailId, "Sender " + lastEmailId++));
            } else if (i%6==0){
                emails.add(new Email("Subject " + lastEmailId, "longestEmailAddressOnTheWeb@woow.ping" + lastEmailId++));
            } else {
                emails.add(new Email("Subject " + lastEmailId, "Sender " + lastEmailId++));
            }
        }
        return emails;
    }

    public static Email createEmail() {
        return new Email("Subject " + lastEmailId, "Sender " + lastEmailId++);
    }

}
