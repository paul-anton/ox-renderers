package ox.renderers;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.pedrogomez.renderers.Renderer;

import ox.renderers.model.Email;

public class EmailRenderer extends Renderer<Email> {

    final static String EMAIL_RENDERER = "EmailRenderer";

    @Bind(R.id.mail_sender)
    TextView sender;

    @Bind(R.id.mail_subject)
    TextView subject;

    @Bind(R.id.view_item_top)
    View swipeableView;

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        //View inflatedView = inflater.inflate(R.layout.view_item_email, parent, false);
        View inflatedView = inflater.inflate(R.layout.view_pizda, parent, false);
        ButterKnife.bind(this, inflatedView);
        return inflatedView;
    }

    @Override
    public void render() {
        Log.i(EMAIL_RENDERER, "render() called");
        Email email = getContent();
        renderSender(email);
        renderSubject(email);
    }

    private void renderSender(Email email) {
        Log.i(EMAIL_RENDERER, "renderSender() called");
        sender.setText(email.getSender());
    }

    @OnClick(R.id.mail_sender)
    void onSenderClicked() {
        Email email = getContent();
        Toast.makeText(getContext(), "Clicked Sender = " + email.getSender(), Toast.LENGTH_LONG)
                .show();
    }

    private void renderSubject(Email email) {
        Log.i(EMAIL_RENDERER, "renderSubject() called");
        subject.setText(email.getSubject());
    }

    @OnClick(R.id.mail_subject)
    void onSubjectClicked() {
        Email email = getContent();
        Toast.makeText(getContext(), "Clicked Subject = " + email.getSubject(), Toast.LENGTH_LONG)
                .show();
    }

    /*@OnClick(R.id.mail_add_button)
    void onAddButtonClicked() {

    }
*/
    public View getSwipeableView() {
        return swipeableView;
    }

    // methods below not needed if using ButterKnife

    @Override
    protected void setUpView(View rootView) {

    }

    @Override
    protected void hookListeners(View rootView) {

    }

}
