package ox.mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class Main extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.try1)
    public void startWebActivity() {
        Intent intent = new Intent(this, WebActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.try2)
    public void startWebActivity2() {
        Intent intent = new Intent(this, WebActivity2.class);
        startActivity(intent);
    }

    @OnClick(R.id.try3)
    public void startWebActivity3() {
        Intent intent = new Intent(this, WebActivity3.class);
        startActivity(intent);
    }

    }
