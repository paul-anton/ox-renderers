package ox.mobile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebActivity extends AppCompatActivity {
    private Callback mCallback = EmptyCallback.INSTANCE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        WebView webView = (WebView) findViewById(R.id.message_content);


        WebSettings webSettings = webView.getSettings();
        boolean supportMultiTouch = getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_TOUCHSCREEN_MULTITOUCH);
        webSettings.setDisplayZoomControls(!supportMultiTouch);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webView.setWebViewClient(new CustomWebViewClient());

        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("file:///android_asset/hello2.html");

    }

    public void buttonClicked(View view) {
        Toast.makeText(this, "Yep", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, WebActivity2.class);
        startActivity(intent);
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return mCallback.onUrlInMessageClicked(url);
        }
    }


    public interface Callback {
        /**
         * Called when a link in a message is clicked.
         *
         * @param url link url that's clicked.
         * @return true if handled, false otherwise.
         */
        public boolean onUrlInMessageClicked(String url);

        /**
         * Called when the message specified doesn't exist, or is deleted/moved.
         */
        public void onMessageNotExists();

        /**
         * Called when it starts loading a message.
         */
        public void onLoadMessageStarted();

        /**
         * Called when it successfully finishes loading a message.
         */
        public void onLoadMessageFinished();

        /**
         * Called when an error occurred during loading a message.
         */
        public void onLoadMessageError(String errorMessage);
    }

    public static class EmptyCallback implements Callback {
        public static final Callback INSTANCE = new EmptyCallback();

        @Override
        public void onLoadMessageError(String errorMessage) {
        }

        @Override
        public void onLoadMessageFinished() {
        }

        @Override
        public void onLoadMessageStarted() {
        }

        @Override
        public void onMessageNotExists() {
        }

        @Override
        public boolean onUrlInMessageClicked(String url) {
            return false;
        }
    }

}
