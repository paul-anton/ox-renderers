package ox.mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web2);
        ObservableWebView webView = (ObservableWebView) findViewById(R.id.webView);
        WebSettings webSetting = webView.getSettings();
        webSetting.setBuiltInZoomControls(true);
        webSetting.setSupportZoom(true);
        webSetting.setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("file:///android_asset/hello2.html");
        webView.setmOnScrollChangedCallback(new ObservableWebView.OnScrollChangedCallback() {
            @Override
            public void onScroll(int l, int t) {

                Toast.makeText(WebActivity2.this, "Scrolling event", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void buttonClicked(View view) {
        Toast.makeText(this, "Yeppp", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, WebActivity3.class);
        startActivity(intent);
    }


}
