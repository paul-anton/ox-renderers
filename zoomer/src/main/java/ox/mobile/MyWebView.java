package ox.mobile;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;

class MyWebView extends WebView {

    public MyWebView(Context context) {
        super(context);
    }

    public MyWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MyWebView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //http://stackoverflow.com/questions/13257990/android-webview-inside-scrollview-scrolls-only-scrollview
        // ^^ but if we do, we lose the ability to scroll inside the webview.
        //requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(event);
    }
}