package rocod.vinebusu;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Generator {

    private static final int VERSION = 2;

    public static final String PACKAGE = "ox.dto";

    public static final String PATH = "./dataset/src/main/java";

    public static void main(String[] args) {
        Schema schema = new Schema(VERSION, PACKAGE);
        addEntities(schema);
        try {
            new DaoGenerator().generateAll(schema, PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addEntities(Schema schema) {
        Entity mockEntityDto = schema.addEntity("MockEntityDto");
        mockEntityDto.addStringProperty("mockSubject");
        mockEntityDto.addStringProperty("mockSender");
        mockEntityDto.addStringProperty("mockDate");
    }

}
